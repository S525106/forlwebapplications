package Spring;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
 @Controller
public class AppController {
	
 @RequestMapping(value="/login.html", method = RequestMethod.GET)
 protected ModelAndView handleRequestInternal(HttpServletRequest request,
         HttpServletResponse response) throws Exception {
	
		ModelAndView modelandview = new ModelAndView("Hello");
		
		 UsShopping us = new UsShopping();
		   us.setPerson(request.getParameter("person"));
		   us.setItem(request.getParameter("item"));
		   
			 SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
				Session session = sessionFactory.openSession();
				session.beginTransaction();

				// this would save the Student_Info object into the database
				session.save(us);
				
				session.getTransaction().commit();
				session.close();
				sessionFactory.close();
		
		return modelandview;
 }
 @ModelAttribute
 public void addingCommonObjects(Model model){
	 model.addAttribute("he","Anwesh's India Shopping List");
	 model.addAttribute("person","Persons");
	 model.addAttribute("items","Items List");
 }
 @RequestMapping(value="/sucessful.html", method= RequestMethod.POST)
 public ModelAndView sucessful(@ModelAttribute("us") UsShopping us){
		ModelAndView modelandview = new ModelAndView("sucessful");
	modelandview.addObject("msg","Your Shopping list");
		
		
		return modelandview;
 }
 
}